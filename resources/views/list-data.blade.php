@extends('layout')

@section('data')
<table class="table-sm table-striped table-bordered table text-center">
    <thead class="thead-dark">
    <tr>
        <th>No</th>
        <th>Nama</th>
        <th>NIM</th>
        <th>Kelas</th>
        <th>Prodi</th>
        <th>Fakultas</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
     @foreach ($mahasiswa as $no=>$data)
    <tr>
        <td>{{ $no+1 }}</td>
        <td>{{ $data->nama_mahasiswa }}</td>
        <td>{{ $data->nim_mahasiswa }}</td> 
        <td>{{ $data->kelas_mahasiswa }}</td>
        <td>{{ $data->prodi_mahasiswa }}</td>
        <td>{{ $data->fakultas_mahasiswa }}</td>
        <td>
            <a href="{{ url('update-data/'.$data->id) }}" class="btn btn-warning">Update</a>
            <a href="{{ url('delete-data/'.$data->id) }}" class="btn btn-danger">Delete</a>
        </td>
    </tr>
    @endforeach
</tbody>
</table>
@endsection