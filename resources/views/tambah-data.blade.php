@extends('layout')

@section('data')
    <form action="list" method="POST">

      @csrf
        <div class="form-group mb-4">
            <label for="exampleFormControlInput1">Nama</label>
            <input type="text" class="form-control" name="nama" placeholder="Masukan Nama Lengkap">
          </div>
          <div class="form-group mb-4">
            <label for="exampleFormControlInput1">NIM</label>
            <input type="text" class="form-control" name="nim" placeholder="Masukan NIM">
          </div>
          <div class="form-group mb-4">
            <label for="exampleFormControlInput1">Kelas</label>
            <input type="text" class="form-control" name="kelas" placeholder="Masukan Kelas">
          </div>
          <div class="form-group mb-4">
            <label for="exampleFormControlInput1">Prodi</label>
            <input type="text" class="form-control" name="prodi" placeholder="Masukan Program Pendidikan">
          </div>
          <div class="form-group mb-4">
            <label for="exampleFormControlInput1">Fakultas</label>
            <input type="text" class="form-control" name="fakultas" placeholder="Masukan Fakultas">
          </div>
          <div class="card-footer text-center mt-5">
            <button class="btn btn-success" type="submit">Tambah Data</button>
          </div>
    </form>
@endsection