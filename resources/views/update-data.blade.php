@extends('layout')

@section('data')
  
    <form action="/actionupdate" method="POST">

      @csrf
      <input type="hidden" name="id" value="{{ $mahasiswa->id }}">
        <div class="form-group mb-4">
            <label for="exampleFormControlInput1">Nama</label>
            <input type="text" class="form-control" value="{{ $mahasiswa->nama_mahasiswa }}" name="nama" placeholder="Masukan Nama Lengkap">
          </div>
          <div class="form-group mb-4">
            <label for="exampleFormControlInput1">NIM</label>
            <input type="text" class="form-control" value="{{ $mahasiswa->nim_mahasiswa }}"name="nim" placeholder="Masukan NIM">
          </div>
          <div class="form-group mb-4">
            <label for="exampleFormControlInput1">Kelas</label>
            <input type="text" class="form-control" value="{{ $mahasiswa->kelas_mahasiswa }}"name="kelas" placeholder="Masukan Kelas">
          </div>
          <div class="form-group mb-4">
            <label for="exampleFormControlInput1">Prodi</label>
            <input type="text" class="form-control" value="{{ $mahasiswa->prodi_mahasiswa }}"name="prodi" placeholder="Masukan Program Pendidikan">
          </div>
          <div class="form-group mb-4">
            <label for="exampleFormControlInput1">Fakultas</label>
            <input type="text" class="form-control" value="{{ $mahasiswa->fakultas_mahasiswa }}"name="fakultas" placeholder="Masukan Fakultas">
          </div>
          <div class="card-footer text-center mt-5">
            <button class="btn btn-success" type="submit">Update</button>
          </div>
    </form>
    @endsection
  