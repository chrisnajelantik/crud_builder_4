<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class homecontrol extends Controller
{
    public function input()
    {
        return view('tambah-data',
        [
            "title" => "Tambah Data"
        ]);
    }

    public function output()
    {
        $mahasiswa = DB::table('mahasiswa')->get();
        return view 
        ('list-data', 
        ['mahasiswa' => $mahasiswa,
         "title" => "List Data"
        ]);
    }

    public function list(Request $request)
    {
        DB::table('mahasiswa')->insert([
            'nama_mahasiswa'=>$request['nama'],
            'nim_mahasiswa'=>$request['nim'],
            'kelas_mahasiswa'=>$request['kelas'],
            'prodi_mahasiswa'=>$request['prodi'],
            'fakultas_mahasiswa'=>$request['fakultas'],
        ]);

        return redirect('list');
    }

    public function update($id){
        $mahasiswa = DB::table('mahasiswa')->where('id',$id)->first();
        return view('update-data',compact('mahasiswa'),
        [
            "title" => "Update Data"
        ]);
    }

    public function actionupdate(Request $request){
        DB::table('mahasiswa')->where('id',$request['id'])->update([
            'nama_mahasiswa'=>$request['nama'],
            'nim_mahasiswa'=>$request['nim'],
            'kelas_mahasiswa'=>$request['kelas'],
            'prodi_mahasiswa'=>$request['prodi'],
            'fakultas_mahasiswa'=>$request['fakultas'],
        ]);
        return redirect('list');
    }
    public function delete($id){
        DB::table('mahasiswa')->where('id',$id)->delete();
        return redirect('list');
    }
}
