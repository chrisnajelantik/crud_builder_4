<?php

use App\Http\Controllers\homecontrol;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/tambah', [homecontrol::class, 'input']);




Route::get('/list', [homecontrol::class, 'output']);
Route::post('/list', [homecontrol::class, 'list']);




Route::get('/update-data/{id}', [homecontrol::class, 'update']);
Route::post('/actionupdate', [homecontrol::class, 'actionupdate']);



Route::get('/delete-data/{id}', [homecontrol::class, 'delete']);



